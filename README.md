# locales

Ansible role that sets Linux locale, localtime, and timezone.

> Definitively, the source of truth for which locales are **available** on the system
> is `/usr/share/i18n/locales/*`. The source of truth for which locales are
> **currently installed** on the system is `locale -a`.
> `/etc/locale.gen` is just a configuration file that should be edited to reference proper
> locales before you try to install them via `locale-gen`.
> https://github.com/ansible/ansible/issues/38931#issuecomment-386563210

## Some diagnostic data


A list of enabled (installed) locales:
```
$ locale -a
C
C.utf8
en_US.utf8
POSIX
sv_SE.utf8
```

```
$ cat /etc/default/locale
LANG=en_US.UTF-8
LC_TIME=sv_SE.UTF-8
```

```
$ grep -v "^#" /etc/locale.gen
en_US.UTF-8 UTF-8
sv_SE.UTF-8 UTF-8
```

The currently set locale and related environmental settings:
```
$ locale
LANG=en_US.UTF-8
LANGUAGE=en
LC_CTYPE="en_US.UTF-8"
LC_NUMERIC="en_US.UTF-8"
LC_TIME=sv_SE.UTF-8
LC_COLLATE="en_US.UTF-8"
LC_MONETARY="en_US.UTF-8"
LC_MESSAGES="en_US.UTF-8"
LC_PAPER="en_US.UTF-8"
LC_NAME="en_US.UTF-8"
LC_ADDRESS="en_US.UTF-8"
LC_TELEPHONE="en_US.UTF-8"
LC_MEASUREMENT="en_US.UTF-8"
LC_IDENTIFICATION="en_US.UTF-8"
LC_ALL=
```

```
$ localectl status
   System Locale: LANG=en_US.UTF-8
                  LC_TIME=sv_SE.UTF-8
       VC Keymap: n/a
      X11 Layout: se
       X11 Model: pc105
```

You can see the specific settings for a category of the locale settings like this:
```
$ locale -c -k LC_TIME
LC_TIME
abday="sön;mån;tis;ons;tor;fre;lör"
day="söndag;måndag;tisdag;onsdag;torsdag;fredag;lördag"
abmon="jan;feb;mar;apr;maj;jun;jul;aug;sep;okt;nov;dec"
mon="januari;februari;mars;april;maj;juni;juli;augusti;september;oktober;november;december"
am_pm=";"
d_t_fmt="%a %e %b %Y %H:%M:%S"
d_fmt="%Y-%m-%d"
t_fmt="%H:%M:%S"
t_fmt_ampm=""
era=
era_year=""
era_d_fmt=""
alt_digits=
era_d_t_fmt=""
era_t_fmt=""
time-era-num-entries=0
time-era-entries="s"
week-ndays=7
week-1stday=19971130
week-1stweek=4
first_weekday=2
first_workday=2
cal_direction=1
timezone=""
date_fmt="%a %e %b %Y %H:%M:%S %Z"
time-codeset="UTF-8"
alt_mon="januari;februari;mars;april;maj;juni;juli;augusti;september;oktober;november;december"
ab_alt_mon="jan;feb;mar;apr;maj;jun;jul;aug;sep;okt;nov;dec"
```



## Links and notes

+ https://andreas.scherbaum.la/blog/archives/941-Configuring-locales-in-Debian-and-Ubuntu,-using-Ansible-Reloaded.html
+ https://serverfault.com/questions/959026/how-do-i-generate-and-set-the-locale-using-ansible
+ https://github.com/infOpen/ansible-role-locales
+ https://github.com/lvancrayelynghe/ansible-ubuntu/tree/master/roles/locales
+ https://www.tecmint.com/set-system-locales-in-linux
+ https://askubuntu.com/questions/193251/how-to-set-all-locale-settings-in-ubuntu
+ https://github.com/ansible/ansible/issues/59647
+ https://stackoverflow.com/questions/52421542/ansible-debconf-module-not-working-properly-to-set-default-locale
+ https://serverfault.com/questions/959026/how-do-i-generate-and-set-the-locale-using-ansible
+ https://forum.ansible.com/t/ansible-ubuntu-locale-keyboard-configuration/786
+ https://github.com/webarch-coop/ansible-role-locale



### Could we use ISO date/time with an English locale?

The international standard date-time format (ISO 8601) is easy to understand,
and my personal preference.
I would like to use it, which I could achieve by changing the entire locale to,
e.g., Swedish, but that comes with some annoying notation, such as decimal commas.

Can Linux help us use ISO 8601 date-time notation with an otherwise
English-language locale?

Others have suggested setting `LC_TIME=en_DK.UTF-8`.
But I tried it, and `en_DK.UTF-8` *is not* ISO 8601. Date-time looks like this:
`16/07/2021, 23.19` (note the slashes instead of dashes, the comma, and the dot
instead of a colon).

I compiled a list of all available English UTF-8 locales on my system:
```
$ grep -i "UTF-8" /usr/share/i18n/SUPPORTED | grep -in "en"
55:ca_ES@valencia UTF-8
80:en_AG UTF-8              # Antigua and Barbuda
81:en_AU.UTF-8 UTF-8        # Australia, ISO 8601-2007 is a national standard
82:en_BW.UTF-8 UTF-8        # Botswana
83:en_CA.UTF-8 UTF-8        # Canada, CAN/CSA-Z234.4-89 (R2007) is a national standard
84:en_DK.UTF-8 UTF-8        # Denmark, DS/ISO 8601:2005 is a national standard
85:en_GB.UTF-8 UTF-8        # Great Britain, BS ISO 8601:2004
86:en_HK.UTF-8 UTF-8        # Hong Kong
87:en_IE.UTF-8 UTF-8        # Ireland, IS/EN 28601:1993 is a national standard
88:en_IL UTF-8              # Israel
89:en_IN UTF-8              # India, IS 7900:2001 is a national standard
90:en_NG UTF-8              # Nigeria
91:en_NZ.UTF-8 UTF-8        # New Zealand
92:en_PH.UTF-8 UTF-8        # Philippines
93:en_SC.UTF-8 UTF-8        # Seychelles
94:en_SG.UTF-8 UTF-8        # Singapore
95:en_US.UTF-8 UTF-8        # United States, ANSI INCITS 30-1997 (R2008) and NIST FIPS PUB 4-2
96:en_ZA.UTF-8 UTF-8        # South Africa, SANS 8601:2009 is a national standard
97:en_ZM UTF-8              # Zambia
98:en_ZW.UTF-8 UTF-8        # Zimbabwe
```

Several of these countries have adopted ISO 8601 as a national standard
(references to standard above from https://en.wikipedia.org/wiki/ISO_8601),
but that does not mean that the international notation is what is defined
in their `locale`.

The table in https://en.wikipedia.org/wiki/Date_format_by_country was also very useful.

I have so far not found any English-language locale that uses proper
ISO 8601 date-time notation.

+ https://joelostblom.com/my-posts/2017-02-08-monday-is-the-first-day-of-the-week
+ https://www.cl.cam.ac.uk/~mgk25/iso-time.html
+ https://help.ubuntu.com/community/Locale
+ https://wiki.archlinux.org/title/Locale
+ https://www.tecmint.com/set-system-locales-in-linux
+ https://lintut.com/how-to-set-up-system-locale-on-ubuntu-18-04
+ https://www.osradar.com/change-or-set-locales-on-linux
+ https://www.gnu.org/software/gettext/manual/gettext.html#Locale-Environment-Variables
+ https://askubuntu.com/questions/918973/how-to-set-lc-time-variable-to-en-dk-while-keeping-en-us-the-system-default-for
+ https://askubuntu.com/questions/162391/how-do-i-fix-my-locale-issue



### ntp errors inside LXC containers

Inside an LXC container, lots of ntpd errors shown when running
`systemctl status` or `journalctl -xe`:

```
Jul 09 14:56:45 shanghai ntpd[244]: adj_systime: Operation not permitted
Jul 09 14:56:46 shanghai ntpd[244]: adj_systime: Operation not permitted
Jul 09 14:56:47 shanghai ntpd[244]: adj_systime: Operation not permitted
Jul 09 15:05:09 shanghai ntpd[244]: local_clock: ntp_loopfilter.c line 818: ntp_adjtime: Operation not permitted
Jul 09 15:05:09 shanghai ntpd[244]: local_clock: ntp_adjtime(TAI) failed: Operation not permitted
Jul 09 15:05:09 shanghai ntpd[244]: local_clock: ntp_loopfilter.c line 848: ntp_adjtime: Operation not permitted
```

When searching the web, most blog posts and forums posts consider how to run
an LXC container that serves time to other devices (i.e., acts as NTP server
on the network), for example, a router running in an LXC container.

But that is quite the opposite of my use case. I would just like to get rid of
the error messages, (which I noticed while debugging mysql, whose systemd job
will not start at the moment, but that's unrelated), and I would not mind
disabling NTP altogether on the LXC containers.

> Running a network time server in an LXC container normally doesn't work even
> if you run the container in privileged mode.
> The reason is that containers drop certain capabilities upon startup for
> security reasons (`sys_module`, `mac_admin`, `mac_override`, `sys_time`).
> https://www.planetcobalt.net/sdb/lxc_time.shtml

**This role uninstalls NTP on LXC containers** (determined using
`ansible_virtualization_type == "lxc"`).

+ https://github.com/lxc/lxd/issues/6566
+ https://linuxdigest.com/howto/run-ntp-inside-a-container
+ https://serverfault.com/questions/340026/ntp-on-main-server-and-alternative-on-virtual-lxc
